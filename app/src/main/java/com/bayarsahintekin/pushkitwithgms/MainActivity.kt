package com.bayarsahintekin.pushkitwithgms

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bayarsahintekin.pushkitdemo.PushHelper

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val pushHelper = PushHelper()
        pushHelper.generateDeviceToken(this)
    }
}
