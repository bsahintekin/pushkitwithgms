package com.bayarsahintekin.pushkitwithgms

import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.hms.aaid.HmsInstanceId

class PushHelper  {
    fun generateDeviceToken(context: Context) {
        Log.i(ContentValues.TAG, "get token: begin")

        // get token
        // get token
        object : Thread() {
            override fun run() {
                try {
                    val appId = AGConnectServicesConfig.fromContext(context).getString("client/app_id")
                    val pushtoken = HmsInstanceId.getInstance(context).getToken(appId, "HCM")
                    Log.e("Push Kit Token:",pushtoken)

                } catch (e: Exception) {
                    Log.i(ContentValues.TAG, "getToken failed, $e")
                }
            }
        }.start()
    }
}