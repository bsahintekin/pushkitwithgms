package com.bayarsahintekin.pushkitwithgms

import android.util.Log
import com.huawei.hms.push.HmsMessageService

class PushKitService  : HmsMessageService() {
    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
        Log.e("Huawei Device Token :", p0)
    }
}